package com.company.lesson2;

public class Human {
    private String name;
    private String patronymic;
    private String surname;

    public Human(String name, String patronymic, String surname) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.patronymic="";
    }

    public String getFullName() {
        return surname+" "+name+" "+patronymic ;
    }
    public String getShortName() {
        return surname+" "+name.charAt(0)+"." + (patronymic.equals("") ? "" : patronymic.charAt(0)+".");
    }
}
