package com.company.lesson2;

public class Main {
    public static void main(String[] args) {


        Human human = new Human("Ivan", "Ivanovich", "Ivanov");
        Human human2 = new Human("Ivan", "Ivanov");
        System.out.println(human.getFullName());
        System.out.println(human.getShortName());
        System.out.println(human2.getFullName());
        System.out.println(human2.getShortName());
    }
}
