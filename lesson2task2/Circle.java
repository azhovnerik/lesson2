package com.company.lesson2task2;

public class Circle {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }
    public boolean containsPoint(Point point) {
        return point.distanceTo(this.getCenter()) <= this.getRadius();
    }
}
