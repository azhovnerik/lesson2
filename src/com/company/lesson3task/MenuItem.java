package com.company.lesson3task;

public interface MenuItem {
    String getName();
    void execute();
    default boolean isFinal(){
        return false;

    }

}
