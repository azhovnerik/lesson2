package com.company.lesson3task;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Circle circle = new Circle() ;
        PointList pointList = new PointList() ;

        Scanner scanner = new Scanner(System.in);
        Menu menu = new Menu(scanner, new MenuItem[]{
                new AddPointMenuItem(pointList,scanner),
                new ChangeCircleMenuItem(circle,scanner),
                new ShowPointsInCircleMenuItem(circle,pointList),
                new ShowPointOutOfCircleMenuItem(circle,pointList),
                new ExitMenuItem(),

        });
        menu.run();

    }
}
