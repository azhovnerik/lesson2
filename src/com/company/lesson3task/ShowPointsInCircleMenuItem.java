package com.company.lesson3task;

public class ShowPointsInCircleMenuItem implements MenuItem {
    private Circle circle;
    private PointList pointList;

    public ShowPointsInCircleMenuItem(Circle circle, PointList pointList) {
        this.circle = circle;
        this.pointList = pointList;
    }

    @Override
    public String getName() {
        return "Show Points In Circle";
    }

    @Override
    public void execute() {
        boolean isMissingData = false;

        if (pointList.getpList() == null) {
            isMissingData = true;
            System.out.println("Не добавлено ни одной точки!");
        } if (circle.isValid()== false) {
            isMissingData = true;
            System.out.println("Не заданы параметры окружности!");
        }
        if (isMissingData) {return;}

            for (Point pp : pointList.getpList()) {
                //  if (pp.distanceTo(circle.getCenter())<=circle.getRadius()){
                if (circle.containsPoint(pp)) {
                    System.out.println(pp.toString());
                }

            }

    }
}
