package com.company.lesson3task;

import java.util.Arrays;

public class PointList {
    private Point[] pList;

    public PointList(Point[] pList) {
        this.pList = pList;
    }

    public PointList() {

    }

    public Point[] getpList() {
        return pList;
    }

    public int length() {
        return pList.length;
    }

    public void addElementToPointList(Point added) {
        if (this.pList !=null){
            Point[] resultPointList = Arrays.copyOf(this.pList, this.pList.length + 1);
            resultPointList[this.pList.length] = added;
            this.pList = resultPointList;}

            else {

            this.pList = new Point[1] ;
            this.pList[0] =  added;
        }

    }
}