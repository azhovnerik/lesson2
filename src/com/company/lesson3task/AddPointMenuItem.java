package com.company.lesson3task;

import java.util.Scanner;

public class AddPointMenuItem implements MenuItem {
    private PointList pointList;
    Scanner scanner;

    public AddPointMenuItem(PointList pointList, Scanner scanner) {
        this.pointList = pointList;
        this.scanner = scanner;
    }

    @Override
    public String getName() {
        return "Add new point";
    }

    @Override
    public void execute() {
        pointList.addElementToPointList(addNewPoint());

    }
    private static Point addNewPoint(){
        double x;
        double y;
        System.out.println(" введите координаты точки");
        System.out.print("x:");
        Scanner  sc = new Scanner(System.in);
        x = sc.nextDouble();
        System.out.print("y:");
        y = sc.nextDouble();
        sc.nextLine();
        return new Point(x,y);
    }

}
