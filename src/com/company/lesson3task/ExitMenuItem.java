package com.company.lesson3task;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {

    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
