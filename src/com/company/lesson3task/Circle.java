package com.company.lesson3task;

public class Circle {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    public Circle() {
    }

    public Point getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }
    public boolean containsPoint(Point point) {
        return point.distanceTo(this.getCenter()) <= this.getRadius();
    }
    public void changeCenter(Point center){
        this.center = center;
    }
    public void changeRadius(double radius){
        this.radius = radius;
    }
    public boolean isValid(){
        if (this.center==null || this.radius==0){
            return false;
        }
        else{
            return true;
        }
    }

}
