package com.company.lesson3task;

import java.util.Scanner;

public class ChangeCircleMenuItem implements MenuItem {
    private Circle circle;
    Scanner scanner;

    public ChangeCircleMenuItem(Circle circle, Scanner scanner) {
        this.circle = circle;
        this.scanner = scanner;
    }

    @Override
    public String getName() {
        return "Change circle";
    }

    @Override
    public void execute() {
        double x;
        double y;
        System.out.println("Введите координаты центра окружности");
        System.out.print("x:");
        x = scanner.nextDouble();
        System.out.print("y:");
        y = scanner.nextDouble();
        this.circle.changeCenter(new Point(x,y));
        System.out.print("Введите радиус окружности:");
        this.circle.changeRadius(scanner.nextDouble());
        //this.circle = new Circle(new Point(x,y),scanner.nextDouble());


    }

}
